package decaf;

import java.io.*;
//import antlr.Token;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import java6035.tools.CLI.*;

class Main {
    public static void main(String[] args) {
        try {
        	CLI.parse (args, new String[0]);

        	InputStream inputStream = args.length == 0 ?
                    System.in : new java.io.FileInputStream(CLI.infile);

        	if (CLI.target == CLI.SCAN) {
        		DecafLexer lexer = new DecafLexer(new ANTLRInputStream(inputStream));
        		Token token;
        		boolean done = false;
        		while (!done) {

				//System.out.println ("oi");
//System.out.println (lexer.nextToken() + "oi");
        			try  {
				
		        		for (token=lexer.nextToken(); token.getType()!=Token.EOF; token=lexer.nextToken()) {

		        			String type = "";
						int type_message = 0;
		        			String text = token.getText();

		        			switch (token.getType()) {
		        				case DecafLexer.ID:
						            type = " IDENTIFIER";
						            break;

							case DecafLexer.STRING:
							    type = " STRINGLITERAL";
							    break;
							
							case DecafLexer.CHAR:
							    type = " CHARLITERAL";
							    break;

							case DecafLexer.UNEXPECTED_CHAR:
							    type = " CHARLITERAsL";
								type_message = 1;

							    break;
		        			}

						switch (type_message) {
							case 0:
								System.out.println (token.getLine() + type + " " + text);
								break;
							case 1:
								System.out.println(CLI.infile.substring(CLI.infile.lastIndexOf('/') + 1) + " line " + token.getLine() + ":" + token.getCharPositionInLine() + ": unexpected char: " + text);
								break;
						}
		        		}
		        		done = true;
        			} catch(Exception e) {
					// print the error:
					//System.out.println ("oi0");
				    	System.out.println(CLI.infile+" "+e);
					//System.out.println ("oi1");
			            	lexer.skip();
			        }
        		}
        	}
        	else if (CLI.target == CLI.PARSE || CLI.target == CLI.DEFAULT)
        	{
        		DecafLexer lexer = new DecafLexer(new ANTLRInputStream(inputStream));
				CommonTokenStream tokens = new CommonTokenStream(lexer);
        		DecafParser parser = new DecafParser(tokens);
                parser.program();
        	}
        	
        } catch(Exception e) {
        	// print the error:
				//System.out.println ("oi2");
            System.out.println(CLI.infile+" "+e);
				//System.out.println ("oi2");
        }
    }

    public static void Error(Token token) {
        System.out.println("Error: " + token.getText());
    }
}

