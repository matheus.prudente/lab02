#!/bin/bash

if [ $# -eq 1 ]; then
	
	SCRIPT=$(readlink -f $0);

	SCRIPT_PATH=`dirname ${SCRIPT}`;

	SCANFILE=$(readlink -f $1);

	SCANFILE_PATH=`dirname ${SCANFILE}`;

	SCANFILE_NAME=`basename ${SCANFILE}`;

	cd ${SCRIPT_PATH};

	echo -e '\n========================= Build =========================\n';
	ant;
	
	echo -e '\n========================= Sourc =========================\n';
	cat ${SCANFILE};

	echo -e '\n========================= Teste =========================\n';
	java -jar dist/Compiler.jar -target scan -debug ${SCANFILE};
	
	echo -e '\n========================= Valid =========================\n';
	cat ${SCANFILE_PATH}/output/${SCANFILE_NAME}.out;
else 
	echo 'Argumentos inválidos';
fi
